//
//  StudentDriver.h
//  PropertyDemo
//
//  Created by James Cash on 04-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DriverProtocol.h"

@interface StudentDriver : NSObject <DriverProtocol>

@end
