//
//  main.m
//  PropertyDemo
//
//  Created by James Cash on 04-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "Car.h"
#import "StudentDriver.h"
#import "KenBlock.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Person *dude = [[Person alloc] initWithName:@"James"];
        [dude displayName];

        NSString *name = dude.name;
        NSLog(@"That name was %@", name);

        [dude setName:@"John"];
        [dude displayName];

        dude.birthday = [[NSDate alloc] init];

        // Protocols
        Car *car = [[Car alloc] init];
        car.driver = [[StudentDriver alloc] init];
//        car.driver = [[KenBlock alloc] init];
        [car drive];

        // dictionary
        NSDictionary *dict = @{@"foo": @"bar"};
        NSDictionary<NSString*, NSDate*>* dict2 = @{@"foo": [[NSDate alloc] init]};
    }
    return 0;
}
