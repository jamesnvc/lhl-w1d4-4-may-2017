//
//  Car.h
//  PropertyDemo
//
//  Created by James Cash on 04-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DriverProtocol.h"

@interface Car : NSObject

// driver is whatever class as long as it conforms to this protocol
@property (nonatomic,strong) id<DriverProtocol> driver;

- (void)drive;

@end
