//
//  Person.m
//  PropertyDemo
//
//  Created by James Cash on 04-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Person.h"

@implementation Person

- (instancetype)initWithName:(NSString*)personName
{
    self = [super init];
    if (self) {
        _name = personName;
    }
    return self;
}

- (void)displayName
{
    NSLog(@"My name is %@", _name);
}

- (NSString *)name {
    return _name;
}

- (void)setName:(NSString *)name
{
    _name = name;
}

- (void)setBirthday:(NSDate *)birthday
{
    NSLog(@"Changing birthday to %@", birthday);
//    self.birthday = birthday; // [self setBirthday:birthday]
    _birthday = birthday;
}


@end
