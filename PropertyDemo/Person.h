//
//  Person.h
//  PropertyDemo
//
//  Created by James Cash on 04-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject {
    NSString *_name;
}

@property (nonatomic,strong) NSDate* birthday;
@property (nonatomic,assign,getter=isBald) BOOL bald;

- (instancetype)initWithName:(NSString*)personName;
- (void)displayName;

- (NSString*)name;
- (void)setName:(NSString*)name;

@end
