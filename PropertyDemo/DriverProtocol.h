//
//  DriverProtocol.h
//  PropertyDemo
//
//  Created by James Cash on 04-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#ifndef DriverProtocol_h
#define DriverProtocol_h


@protocol DriverProtocol <NSObject>
@required

- (void)doDrivingThing;

@optional

- (NSInteger)maxSpeed;

@end

@protocol StuntDrivingProtocol <NSObject,DriverProtocol>

@required

- (void)doSickWheelie;

@end

#endif /* DriverProtocol_h */
