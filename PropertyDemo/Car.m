//
//  Car.m
//  PropertyDemo
//
//  Created by James Cash on 04-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Car.h"

@implementation Car

- (void)drive
{
    [self.driver doDrivingThing];
    // guard because the maxSpeed method is optional
    if ([self.driver respondsToSelector:@selector(maxSpeed)]) {
        NSInteger speed = [self.driver maxSpeed];
        // set throttle to some position based on speed from delegate
        
    }
}

@end
